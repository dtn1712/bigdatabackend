# PURIFY SERVER CODE
This repo store the codes of Purify's Maven-based server.

## Requirements before running
- Java 1.7
- JAVA_HOME points to correct Java 1.7 Home
- Maven

## How to build server
mvn clean compile exec:java

