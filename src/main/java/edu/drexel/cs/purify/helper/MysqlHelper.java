package edu.drexel.cs.purify.helper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlHelper {

    private static final Logger logger = LoggerFactory.getLogger(MysqlHelper.class);

	private static MysqlHelper helper = null;
    private Connection con = null;

    /*private static final String DROP_TERM = "DROP TABLE Terms;";
    private static final String DROP_TERMS_MESSAGES = "DROP TABLE Term_Message;";

    private static final String CREATE_TERM = "CREATE TABLE IF NOT EXISTS Terms (id MEDIUMINT PRIMARY KEY AUTO_INCREMENT, keywords TEXT NOT NULL, lat DOUBLE, lon DOUBLE, radius DOUBLE, source VARCHAR(255));";
    private static final String CREATE_TERMS_MESSAGES = "CREATE TABLE IF NOT EXISTS Term_Message (id MEDIUMINT PRIMARY KEY AUTO_INCREMENT, term_id MEDIUMINT NOT NULL, message_id MEDIUMINT NOT NULL);";
    private static final String CREATE_ANALYZED_MESSAGE = "CREATE TABLE IF NOT EXISTS AnalyzedMessages ("
    		+ "id MEDIUMINT PRIMARY KEY AUTO_INCREMENT, "
    		+ "user_id VARCHAR(50), message VARCHAR(255) NOT NULL, link VARCHAR(255), photo VARCHAR(255), "
    		+ "lat DOUBLE, lon DOUBLE, timestamp BIGINT, source VARCHAR(50), sentiment INT NOT NULL )";*/

	public static MysqlHelper getHelper() throws ClassNotFoundException, SQLException {
		if (helper == null) {
			helper = new MysqlHelper();
		}
		return helper;
	}
	
	private MysqlHelper() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://"+System.getProperty("db.host")+":"+System.getProperty("db.port")+"/"+System.getProperty("db.database")+"?useUnicode=true";
		con = DriverManager.getConnection(url, System.getProperty("db.username"), System.getProperty("db.password"));
        initialize();
	}
	
	public Connection getConnection() {
		return con;
	}
	
	/**
	 * Execute INSERT/CREATE sql query
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public boolean execute(String sql) throws SQLException{
		return con.createStatement().execute(sql);
	}
	
	/**
	 * Execute SELECT query
	 * @param sql
	 * @return the Results
	 * @throws SQLException
	 */
	public ResultSet executeQuery(String sql) throws SQLException{
		ResultSet resultSet = con.createStatement().executeQuery(sql);
		return resultSet;
	}
	
	/**
	 * Initialize all necessary tables
	 * @throws SQLException
	 */
	public void initialize() throws SQLException {
		/*execute(DROP_TERM);
		execute(DROP_TERMS_MESSAGES);
		execute(CREATE_TERM);
		execute(CREATE_TERMS_MESSAGES);
        execute(CREATE_ANALYZED_MESSAGE);*/
	}
	
	public static void main(String args[]) throws ClassNotFoundException, SQLException{
		MysqlHelper helper = getHelper();
		/*
		helper.execute("Insert into Terms (keywords) values ('hunger,game');");
		helper.execute("Insert into Terms (keywords) values ('hunger,games');");
		helper.execute("Insert into Terms (keywords, lat, lon, radius) values ('tesla,model s', 123456.78, 123456.78, 100);");
		helper.execute("Insert into Terms (keywords) values ('tesla,car');");*/
		ResultSet res = helper.executeQuery("select * from terms");
		logger.info("Result");
		while(res.next()) {
			logger.info(res.getInt(1)+"\t"+res.getString(2)+"\t"+res.getDouble(3)+"\t"+res.getDouble(4)+"\t"+res.getDouble(5));
		}
	}
}
