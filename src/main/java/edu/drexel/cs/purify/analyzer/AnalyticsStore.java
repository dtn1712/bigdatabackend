package edu.drexel.cs.purify.analyzer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.drexel.cs.purify.Constants;
import edu.drexel.cs.purify.analyzer.impl.GeolocationAnalyzer;
import edu.drexel.cs.purify.analyzer.impl.InfluenceAnalyzer;
import edu.drexel.cs.purify.analyzer.impl.LinkAnalyzer;
import edu.drexel.cs.purify.analyzer.impl.SentimentAnalyzer;
import edu.drexel.cs.purify.helper.MysqlHelper;
import edu.drexel.cs.purify.server.models.NormalizedMessage;
import edu.drexel.cs.purify.server.models.Query;

public class AnalyticsStore {

    private Analyzer[] analyzers;
	private SentimentAnalyzer sentimentAnalyzer;
    private InfluenceAnalyzer influenceAnalyzer;
    private GeolocationAnalyzer geolocationAnalyzer;
	private LinkAnalyzer linkAnalyzer; 
    private static final Logger logger = LoggerFactory.getLogger(AnalyticsStore.class);

    private static AnalyticsStore ourInstance = new AnalyticsStore();
    private static final String INSERT_ANALYZED_MESSAGE = "INSERT INTO messages (username, message, latitude, longitude, posted_at, sentiment, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?);";
    private static final String INSERT_TERM_MESSAGE = "INSERT INTO messages_terms (term_id, message_id) VALUES ('%s', '%s');";
    
    public static AnalyticsStore getInstance() {
        if (ourInstance == null) {
        	ourInstance = new AnalyticsStore();
		}
		return ourInstance;
    }

    private AnalyticsStore() {
    	sentimentAnalyzer = new SentimentAnalyzer();
    }
    
    /**
     * The main entrance of Analyzer that will run all kind of analyzers
     * For this prototype, we run sentiment analyzer only
     * @param msg
     */
    public void run(NormalizedMessage msg) {
    	AnalyzerResult result = sentimentAnalyzer.process(msg);
    	if (result!=null) {
    		save(result);
    	}
    }

    /**
     * Save analyzed message to db
     * Save the relations between terms and message
     * @param analyzerResult
     */
    private void save(AnalyzerResult analyzerResult) {
        org.apache.log4j.PropertyConfigurator.configure(Constants.LOG_PROPERTIES);
    	NormalizedMessage msg = analyzerResult.getAnalyzedMessage();
        try {
        	MysqlHelper helper = MysqlHelper.getHelper();
            Connection con = helper.getConnection();
            PreparedStatement stmt = con.prepareStatement(INSERT_ANALYZED_MESSAGE, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, msg.user_id);
            stmt.setString(2, msg.content);
            stmt.setDouble(3, msg.lat);
            stmt.setDouble(4, msg.lon);
            stmt.setTimestamp(5, new Timestamp(msg.timestamp));
            stmt.setObject(6, analyzerResult.getResult());
            stmt.setTimestamp(7, new Timestamp(msg.timestamp));
            stmt.setTimestamp(8, new Timestamp(msg.timestamp));
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
            	logger.info("Thread"+Thread.currentThread().getId()+": "+msg.content);
                int message_id = rs.getInt(1);
                ArrayList<Query> matchedTerms = msg.matchedTerms;
                String st = "--- is matched with terms: ";
                for(Query term : matchedTerms) {
	                String query = String.format(INSERT_TERM_MESSAGE, term.id, message_id);
	                st += term.keywords.toString()+", ";
	                helper.execute(query);
                }
                logger.info(st);
            }
            logger.info("--- sentiment result: "+analyzerResult.getResult());
//            logger.info(String.format("Store analyzed message '%s' successfully",analyzerResult.getAnalyzedMessage()));
        } catch (ClassNotFoundException e) {
            logger.error(String.format("Exceptions storing analyzed message '%s'",analyzerResult.getAnalyzedMessage()),e);
        } catch (SQLException e) {
            logger.error(String.format("Exceptions storing analyzed message '%s'",analyzerResult.getAnalyzedMessage()),e);
        }
    }
}
