package edu.drexel.cs.purify.analyzer.impl;

import java.util.Properties;

import edu.drexel.cs.purify.analyzer.AnalyticsStore;
import edu.drexel.cs.purify.analyzer.Analyzer;
import edu.drexel.cs.purify.analyzer.AnalyzerResult;
import edu.drexel.cs.purify.server.models.NormalizedMessage;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

public class GeolocationAnalyzer implements Analyzer{

    private AnalyzerResult analyzerResult;
    private String message;
    private StanfordCoreNLP pipeline;
    
    public GeolocationAnalyzer() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }

    @Override
    public AnalyzerResult process(NormalizedMessage normalizedMessage) {
        this.message = normalizedMessage.content;

        int mainSentiment = 0;
        if (message != null && message.length() > 0) {
            int longest = 0;
            Annotation annotation = pipeline.process(message);
            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = sentence.toString();
                if (partText.length() > longest) {
                    mainSentiment = sentiment;
                    longest = partText.length();
                }

            }
        }
        if (mainSentiment > 4 || mainSentiment < 0) {
            return null;
        }
        analyzerResult = new SentimentResult(mainSentiment,normalizedMessage);
        return analyzerResult;
    }

    @Override
    public void push() {
    }


}
