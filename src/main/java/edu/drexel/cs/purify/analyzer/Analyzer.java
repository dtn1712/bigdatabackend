package edu.drexel.cs.purify.analyzer;

import edu.drexel.cs.purify.server.models.NormalizedMessage;

public interface Analyzer {

    AnalyzerResult process(NormalizedMessage input);

    void push();

}
