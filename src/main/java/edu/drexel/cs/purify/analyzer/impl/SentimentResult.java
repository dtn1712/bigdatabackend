package edu.drexel.cs.purify.analyzer.impl;

import edu.drexel.cs.purify.analyzer.AnalyzerResult;
import edu.drexel.cs.purify.server.models.NormalizedMessage;

public class SentimentResult extends AnalyzerResult {

    public int sentimentValue;
    private NormalizedMessage analyzedMessage;

    public SentimentResult() {}

    public SentimentResult(int sentimentValue,NormalizedMessage analyzedMessage) {
        this.sentimentValue = sentimentValue;
        this.analyzedMessage = analyzedMessage;
    }

    public int getSentimentValue() {
        return sentimentValue;
    }

    public void setSentimentValue(int sentimentValue) {
        this.sentimentValue = sentimentValue;
    }

    public void setAnalyzedMessage(NormalizedMessage analyzedMessage) {
        this.analyzedMessage = analyzedMessage;
    }

    @Override
    public NormalizedMessage getAnalyzedMessage() {
        return analyzedMessage;
    }

    @Override
    public Object getResult() {
        return this.sentimentValue;
    }
}
