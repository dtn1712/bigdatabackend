package edu.drexel.cs.purify.analyzer;

import edu.drexel.cs.purify.server.models.NormalizedMessage;


public abstract class AnalyzerResult {

    public Object getResult() {return null;}

    public NormalizedMessage getAnalyzedMessage() {return null;}

}
