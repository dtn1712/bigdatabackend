package edu.drexel.cs.purify;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import edu.drexel.cs.purify.helper.MysqlHelper;
import edu.drexel.cs.purify.server.StreamManager;
import edu.drexel.cs.purify.server.TwitterStreamReader;
import edu.drexel.cs.purify.server.models.Source;
import edu.drexel.cs.purify.server.models.Query;

// TODO: peacefully start and stop the server
public class Main extends AbstractHandler
{

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public void handle(String target,
                       Request baseRequest,
                       HttpServletRequest request,
                       HttpServletResponse response) 
        throws IOException, ServletException
    {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        response.getWriter().println("<h1>Hello World</h1>");
    }
 
    public static void main(String[] args) throws Exception
    {
        org.apache.log4j.PropertyConfigurator.configure(Constants.LOG_PROPERTIES);
    	MysqlHelper helper = MysqlHelper.getHelper();
//		helper.initialize();
//		helper.execute("Insert into Terms (keywords) values ('hunger,games');");
//		helper.execute("Insert into Terms (keywords, lat, lon, radius) values ('hunger', 123456.78, 123456.78, 100);");
//		helper.execute("Insert into Terms (keywords) values ('tesla');");
		ResultSet res = helper.executeQuery("select * from Terms");
		ArrayList<Query> terms = new ArrayList<Query>();
		logger.info("Terms table:");
		logger.info("id\tkeywords\tlat\tlon\tradius");
		while(res.next()) {
			logger.info(res.getInt(1)+"\t"+res.getString(2)+"\t"+res.getDouble(3)+"\t"+res.getDouble(4)+"\t"+res.getDouble(5));
			terms.add(new Query.Builder().setLat(res.getDouble(4)).setSource(Source.Twitter).setLon(res.getDouble(4)).addKeyword(res.getString(2)).build());
		}
    	// TODO: auto-wired
    	StreamManager streamManager = StreamManager.getInstance();
    	// TODO: autowired
    	TwitterStreamReader twitter = new TwitterStreamReader();
    	streamManager.onStart(terms);
    }
}