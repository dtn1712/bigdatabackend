package edu.drexel.cs.purify.server;

import java.util.ArrayList;
import java.util.List;

import edu.drexel.cs.purify.server.models.Query;

/**
 * Purify
 * @author dan
 * Singleton class, use to store and call all stream readers
 */
public class StreamManager implements QueryListener, ServerListener{
	/**
	 * All streams must be created and started and registered to StreamManager when server starts
	 */
	private final ArrayList<StreamReader> streams;
	
	private StreamManager() {
		streams = new ArrayList<StreamReader>();
	}
	
	private static class Singleton {
		public static final StreamManager streamManager = new StreamManager();
	}
	
	public static StreamManager getInstance() {
		return Singleton.streamManager;
	}
	
	public void addStream(StreamReader stream) {
		streams.add(stream);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQueryChange(List<Query> termsAdded, List<Query> termsRemoved) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(List<Query> terms) {
		// TODO Auto-generated method stub
		for (StreamReader stream:streams) 
			stream.onStart(terms);
	} 
}
