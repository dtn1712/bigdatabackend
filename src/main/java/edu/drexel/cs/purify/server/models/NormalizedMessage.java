package edu.drexel.cs.purify.server.models;

import java.util.ArrayList;

import edu.drexel.cs.purify.analyzer.AnalyzerResult;

public class NormalizedMessage {
	public String user_id;
	public String content;
	public String link;
	public String photo;
	public Source source;
	public double lat;
	public double lon;
	public long timestamp;
	public ArrayList<Query> matchedTerms;
	public ArrayList<AnalyzerResult> results;
	
	private NormalizedMessage(String user_id, String content, String link, String photo, Source source, 
			double lat, double lon, long timestamp, ArrayList<Query> matchedTerms, ArrayList<AnalyzerResult> results) {
		this.user_id = user_id;
		this.content = content;
		this.link = link;
		this.photo = photo;
		this.lat = lat;
		this.lon = lon;
		this.timestamp = timestamp;
		this.matchedTerms = matchedTerms;
		this.results = results;
		this.source = source;
	}
	
	public static class Builder {
		private String user_id;
		private String content;
		private String link;
		private String photo;
		private Source source;
		private double lat;
		private double lon;
		private long timestamp;
		private ArrayList<Query> matchedTerms;
		private ArrayList<AnalyzerResult> results;
		public Builder() { matchedTerms = new ArrayList<Query>(); results = new ArrayList<AnalyzerResult>();}
		public Builder setUserId(String id) {this.user_id = id; return this;}
		public Builder setContent(String content) {this.content = content; return this;}
		public Builder setLink(String link) {this.link = link; return this;}
		public Builder setPhoto(String photo) {this.photo = photo; return this;}
		public Builder setTimestamp(long timestamp) {this.timestamp = timestamp; return this;}
		public Builder setLat(double lat) {this.lat = lat; return this;}
		public Builder setLon(double lon) {this.lon = lon; return this;}
		public Builder addMatchedTerms(Query matchedTerm) {this.matchedTerms.add(matchedTerm); return this;}
		public Builder addResults(AnalyzerResult result) {this.results.add(result); return this;}
		public Builder setSource(Source source) {this.source = source; return this;}
		public NormalizedMessage build() {
			return new NormalizedMessage(user_id, content, link, photo, source, lat, lon, timestamp, matchedTerms, results);
		}
	}
}
