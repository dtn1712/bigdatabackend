package edu.drexel.cs.purify.server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.management.RuntimeErrorException;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.twitter.hbc.twitter4j.Twitter4jStatusClient;

import edu.drexel.cs.purify.normalizer.TwitterNormalizer;
import edu.drexel.cs.purify.server.models.Query;

/**
 * Purify
 * @author dan
 * This class loads terms from term manager and requests Twitters for tweets. This must be a singleton class,
 * only one connection to Twitter is allowed. The connection will be restarted when there is a change in terms
 * 
 */
public class TwitterStreamReader extends StreamReader {
	
	private final String key, secret, token, tokensecret;
	private final List<Query> terms;
	private final TwitterNormalizer tweetConsumer;
	private Twitter4jStatusClient statusClient;
	
	// TODO: this class should use Spring for initialization
	/**
	 * The constructor will retrieve all credential requirements for Twitter.
	 */
	public TwitterStreamReader() {
		key = System.getProperty("twitter.key");
		secret = System.getProperty("twitter.secret");
		token = System.getProperty("twitter.token");
		tokensecret = System.getProperty("twitter.token_secret");
		if (key == null || secret == null) throw new RuntimeErrorException(null, "Twitter Credentials not found");
		terms = new ArrayList<Query>();
		StreamManager.getInstance().addStream(this);      
        tweetConsumer = new TwitterNormalizer();
	}
	

	@Override
	public void onStart(List<Query> terms) {
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
		StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
		this.terms.addAll(terms);
		List<String> keywords = new ArrayList<String>();
		for (Query t:terms) keywords.addAll(t.keywords);
		endpoint.trackTerms(keywords);
		
		Authentication auth = new OAuth1(key, secret, token, tokensecret);
		BasicClient client = new ClientBuilder()
			.hosts(Constants.STREAM_HOST)
			.endpoint(endpoint)
			.authentication(auth)
			.processor(new StringDelimitedProcessor(queue))
			.build();
		ExecutorService service = Executors.newFixedThreadPool(4);

		statusClient = new Twitter4jStatusClient(client, queue, Lists.newArrayList(tweetConsumer), service);
		statusClient.connect();
		for (int t=0; t<4; t++) {
			statusClient.process();
		}
	}

	@Override
	public void onStop() {
		statusClient.stop();
	}

	@Override
	public void onQueryChange(List<Query> termsAdded, List<Query> termsRemoved) {
		// TODO Auto-generated method stub
		
	}

}
