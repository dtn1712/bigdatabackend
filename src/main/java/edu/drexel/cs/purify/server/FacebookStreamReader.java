package edu.drexel.cs.purify.server;

import java.util.List;

import edu.drexel.cs.purify.normalizer.FacebookNormalizer;
import edu.drexel.cs.purify.server.models.Query;

/**
 * This class will take any post/comment from a FacebookStream
 * and pass it to FacebookPostNormalizer or FacebookCommentNormalizer
 *
 */
public class FacebookStreamReader extends StreamReader{
    
    private final FacebookNormalizer facebookConsumer;
    
    public FacebookStreamReader() {
        facebookConsumer = new FacebookNormalizer();
    }

    @Override
    public void onQueryChange(List<Query> termsAdded, List<Query> termsRemoved) {
        
    }

    @Override
    public void onStart(List<Query> term) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        
    }
    
}
