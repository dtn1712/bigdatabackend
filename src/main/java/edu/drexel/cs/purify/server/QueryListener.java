package edu.drexel.cs.purify.server;

import java.util.List;

import edu.drexel.cs.purify.server.models.Query;

/**
 * Purify
 * @author dan
 * all classes acting on term must implement this interface
 */
public interface QueryListener {
	public void onQueryChange(List<Query> termsAdded, List<Query> termsRemoved);
}
