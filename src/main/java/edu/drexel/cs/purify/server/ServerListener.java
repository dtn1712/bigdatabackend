package edu.drexel.cs.purify.server;

import java.util.List;

import edu.drexel.cs.purify.server.models.Query;

/**
 * Purify
 * @author dan
 * All thread-based servers need to implement this interface
 * and listen to status of the main server
 */
public interface ServerListener {
	/**
	 * main server is up, all setup should be started in this method
	 */
	public void onStart(List<Query> term);
	
	/**
	 * main server is shutting down, your listener needs to clean up and close any connections/ports
	 */
	public void onStop();
	
}
