package edu.drexel.cs.purify.server.models;

import java.util.ArrayList;

/**
 * Purify
 * @author dan
 * This class defines a term, pulled from our general database
 */
public class Query {
	public final int id;
	public final ArrayList<String> keywords;
	public final double lat;
	public final double lon;
	public final double radius;
	public final Source source;
	
	private Query(int id, ArrayList<String> keywords, double lat, double lon, double radius, Source source) {
		this.id = id;
		this.keywords = keywords;
		this.lat = lat;
		this.lon = lon;
		this.radius = radius;
		this.source = source;
	}
	
	/**
	 * Check if the message is matched with current term
	 * @param message
	 * @return T/F
	 */
	public boolean isMatchedWith(NormalizedMessage message) {
		boolean matched = false;
		String msgContent = message.content;
		for (String keyword : keywords) {
			if (msgContent.toLowerCase().contains(keyword.toLowerCase())) {
				matched = true;
				break;
			}
		}
		// If content doesn't contain any keyword, return false immediately
		if (!matched) return false;
		if (lat == 0 && lon == 0) return true;
		// TODO: if message does not have geo-location, what should be returned?
		double distance = Math.sqrt(Math.pow(message.lat-lat, 2)+Math.pow(message.lon-lon, 2));
		if (distance > radius) return false;
		
		return true;
	}

	public static class Builder {
		private int id;
		private ArrayList<String> keywords;
		private double lat;
		private double lon;
		private double radius;
		private Source source;
		public Builder() { keywords = new ArrayList<String>();}
		public Builder setId(int id) {this.id = id; return this;}
		public Builder addKeyword(String string) {this.keywords.add(string); return this;}
		public Builder setLat(double lat) {this.lat = lat; return this;}
		public Builder setLon(double lon) {this.lon = lon; return this;}
		public Builder setRadius(double radius) {this.radius = radius; return this;}
		public Builder setSource(Source source) {this.source = source; return this;}
		public Query build() {
			return new Query(id, keywords, lat, lon, radius, source);
		}
	}
	
}
