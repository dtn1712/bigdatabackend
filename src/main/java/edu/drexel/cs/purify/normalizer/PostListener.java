package edu.drexel.cs.purify.normalizer;

public interface PostListener {
    public void onPost(FacebookPost post);
}
