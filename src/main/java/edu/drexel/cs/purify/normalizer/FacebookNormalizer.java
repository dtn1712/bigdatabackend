package edu.drexel.cs.purify.normalizer;

import java.util.ArrayList;

import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import edu.drexel.cs.purify.analyzer.AnalyticsStore;
import edu.drexel.cs.purify.server.models.NormalizedMessage;
import edu.drexel.cs.purify.server.models.NormalizedMessage.Builder;
import edu.drexel.cs.purify.server.models.Source;
import edu.drexel.cs.purify.server.models.Query;

public class FacebookNormalizer implements Normalizer, PostListener {
    
    private final AnalyticsStore analyzers;
    
    public FacebookNormalizer() {
        analyzers = AnalyticsStore.getInstance();
    } 

	/**
	 * Convert Status object from Twitter to normalized message
	 * @param Status object from Twitter
	 * @return Normalized message
	 */
	public NormalizedMessage normalize(FacebookPost arg0) {
		NormalizedMessage newMessage = null;
		
		return newMessage;
	}
	
	@Override
	public void onReceiveMessage(NormalizedMessage message) {
		ArrayList<Query> allTerms = QueryManager.getQueries();
		boolean isMatched = false;
		for (Query term : allTerms) {
			if (term.isMatchedWith(message)) {
				message.matchedTerms.add(term);
				isMatched = true;
			}
		}
		
		if (isMatched) {
//			System.out.println("--- is matched with terms: ");
//			for (Term term : message.matchedTerms) {
//				System.out.println(term.keywords.toString());
//			}
			AnalyticsStore analyzers = AnalyticsStore.getInstance();
			analyzers.run(message);
		} else {
			System.out.println("Not matched with any term");
		}
		
		// TODO: if isMatched, save message to db and relations of msg and terms
		// TODO: or send the normalized message to Analyzer and let it save msg to db
	}

	/**
	 * Handle when a new status comming in from Twitter
	 */
	@Override
	public void onPost(FacebookPost arg0) {
//		System.out.println("Thread"+Thread.currentThread().getId() + ": " + arg0.getText());
		NormalizedMessage newMessage = normalize(arg0);
		onReceiveMessage(newMessage);
	}

}
