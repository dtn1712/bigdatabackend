package edu.drexel.cs.purify.normalizer;

import edu.drexel.cs.purify.server.models.NormalizedMessage;


/**
 * Purify
 * @author thachnh
 * Interface of Status Handler, converting raw data from diff sources to normalized messages
 */
public interface Normalizer {
	
	/**
	 * Handle the event of receiving new NormalizedMessage, which matches that NormalizedMessage with queries and pass it to Data Analyzer
	 * @param message The NormalizedMessage that Data Stream Reader passes to Data Normalizer
	 */
	void onReceiveMessage(NormalizedMessage message);
	
}
