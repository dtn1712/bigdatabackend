package edu.drexel.cs.purify.normalizer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import edu.drexel.cs.purify.helper.MysqlHelper;
import edu.drexel.cs.purify.server.models.NormalizedMessage;
import edu.drexel.cs.purify.server.QueryListener;
import edu.drexel.cs.purify.server.models.Source;
import edu.drexel.cs.purify.server.models.Query;
import edu.drexel.cs.purify.server.models.Query.Builder;

/**
 * Purify
 * @author thachnh
 * Temp manager to manage incoming term from front-end, and return terms in db
 */
public class QueryManager {
    
    QueryListener[] listeners;
	
	/**
	 * This method will handle the new query coming in from the front-end.
	 * @param keywords list of keywords in the query
	 * @param lat latitude of the central point that defines the search area 
	 * @param lon longitude of the central point that defines the search area 
	 * @param radius radius search area around the central point 
	 * @param source specified source (Twitter/Facebook)
	 * @return The new query's id
	 */
	public static int onReceiveQuery(ArrayList<String> keywords, float lat, float lon, float radius, Source source) {
		// TODO: if duplicated in db, return the id in db
		// TODO: if not, push Term to db (should we created Term obj first?)
		// TODO: return the id of Term
		return 0;
	}
	
	/**
	 * This method will retrieve all queries in the database.
	 * @return List of queries
	 */
	public static ArrayList<Query> getQueries() {
		ArrayList<Query> list = new ArrayList<Query>();
		
		MysqlHelper helper;
		try {
			helper = MysqlHelper.getHelper();
			ResultSet res = helper.executeQuery("select * from Terms");
			while(res.next()) {
				Builder builder = new Query.Builder().setSource(Source.Twitter).setId(res.getInt(1))
						.setLat(res.getDouble(3)).setLon(res.getDouble(4)).setRadius(res.getDouble(5));
				String[] keywords = res.getString(2).split(",");
				for (String key : keywords) builder.addKeyword(key);
				list.add(builder.build());
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * This method will return all queries associated with a particular user
	 * @param user_id the user's id
	 * @return List of queries
	 */
	public static ArrayList<Query> getQueries(int user_id) {
		return null;
	}
	
	/**
	 * This method will delete a particular query
	 * @param query_id the query's id
	 */
	public static void deleteQuery(int query_id) {
		
	}
	
	/**
	 * This method will deactivate a particular query
	 * @param query_id the query's id
	 */
	public static void deactivateQuery(int query_id) {
		
	}
	
	/**
	 * This method will retrieve list of Normalized Messages associated with a query
	 * @param query_id the query's id
	 * @return List of NormalizedMessages
	 */
	public static ArrayList<NormalizedMessage> getData(int query_id) {
		return null;
	}
}
