package edu.drexel.cs.purify.normalizer;

import java.util.ArrayList;

import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import edu.drexel.cs.purify.analyzer.AnalyticsStore;
import edu.drexel.cs.purify.server.models.NormalizedMessage;
import edu.drexel.cs.purify.server.models.NormalizedMessage.Builder;
import edu.drexel.cs.purify.server.models.Source;
import edu.drexel.cs.purify.server.models.Query;

public class TwitterNormalizer implements Normalizer, StatusListener {
    
    private final AnalyticsStore analyzers;
    
    public TwitterNormalizer() {
        analyzers = AnalyticsStore.getInstance();
    }
    

	/**
	 * Convert Status object from Twitter to normalized message
	 * @param Status object from Twitter
	 * @return Normalized message
	 */
	public NormalizedMessage normalize(Status status) {
		Builder builder = new NormalizedMessage.Builder()
								.setSource(Source.Twitter).setUserId(status.getUser().getName())
								.setContent(status.getText()).setTimestamp(status.getCreatedAt().getTime());
		if (status.getGeoLocation()!=null) {
			builder.setLat(status.getGeoLocation().getLatitude());
			builder.setLon(status.getGeoLocation().getLongitude());
		}
		if (status.getURLEntities()!=null) {
			String links = "";
			for (int i = 0; i < status.getURLEntities().length; i++) links+=status.getURLEntities()[i].getDisplayURL()+"|";
			builder.setLink(links);
		}
		if (status.getMediaEntities()!=null) {
			String medias = "";
			for (int i = 0; i < status.getMediaEntities().length; i++) medias+=status.getMediaEntities()[i].getDisplayURL()+"|";
			builder.setPhoto(medias);
		}
		
		NormalizedMessage newMessage = builder.build();
		
		return newMessage;
	}
	
	@Override
	public void onReceiveMessage(NormalizedMessage message) {
		ArrayList<Query> allTerms = QueryManager.getQueries();
		boolean isMatched = false;
		for (Query term : allTerms) {
			if (term.isMatchedWith(message)) {
				message.matchedTerms.add(term);
				isMatched = true;
			}
		}
		
		if (isMatched) {
//			System.out.println("--- is matched with terms: ");
//			for (Term term : message.matchedTerms) {
//				System.out.println(term.keywords.toString());
//			}
			analyzers.run(message);
		} else {
			System.out.println("Not matched with any term");
		}
		
		// TODO: if isMatched, save message to db and relations of msg and terms
		// TODO: or send the normalized message to Analyzer and let it save msg to db
	}
	
	@Override
	public void onException(Exception arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Handle when a new status comming in from Twitter
	 */
	@Override
	public void onStatus(Status arg0) {
//		System.out.println("Thread"+Thread.currentThread().getId() + ": " + arg0.getText());
		NormalizedMessage newMessage = normalize(arg0);
		onReceiveMessage(newMessage);
	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		// TODO Auto-generated method stub
		
	}

}
