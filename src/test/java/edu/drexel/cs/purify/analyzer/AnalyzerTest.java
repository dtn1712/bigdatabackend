package edu.drexel.cs.purify.analyzer;

import edu.drexel.cs.purify.analyzer.impl.SentimentAnalyzer;
import edu.drexel.cs.purify.helper.MysqlHelper;
import edu.drexel.cs.purify.server.models.NormalizedMessage;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;


public class AnalyzerTest {

    private static final Logger logger = LoggerFactory.getLogger(AnalyzerTest.class);

    private static Analyzer analyzer;
    private static AnalyzerResult analyzerResult;

    @BeforeClass
    public static void setUp() {
        analyzer = new SentimentAnalyzer();
    }

    @AfterClass
    public static void tearDown() {
        analyzer = null;
        analyzerResult = null;
    }

    @Test
    public void testAnalyzer() {
    	NormalizedMessage message = new NormalizedMessage.Builder().setContent("My brother that slammed the phone down on me when I said, \"Let's build that Tesla warp drive Michael!\"").build();
        analyzerResult = analyzer.process(message);
        assertTrue((int) analyzerResult.getResult() >= 0 || (int) analyzerResult.getResult() <= 4);
        
    	message = new NormalizedMessage.Builder().setContent("Looking forward to riding in the #Tesla Roadster").build();
        analyzerResult = analyzer.process(message);
        assertTrue((int) analyzerResult.getResult() == 2);
    }

}
